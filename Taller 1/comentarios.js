    document.getElementById("caja").onsubmit = comentar;  
    var validacionA = document.getElementById("validacion1");
    var validacionB = document.getElementById("validacion2");
    let comentarios = [];

    document.getElementById('mensaje').onkeyup = function () {
        document.getElementById('caracteres').innerHTML = this.value.length + "/200";
      };

    function validarAutor(){
        var autor = document.getElementById("autor").value;
        if(autor.length >= 3){
            validacionA.innerHTML = "";
            return true;
        }else if(autor.length < 3){
            validacionA.style.color = "red";
            validacionA.innerHTML = "El nombre del autor debe tener al menos 3 carácteres";
            return false;
        }
    }

    function validarMensaje(){
        var mensaje = document.getElementById("mensaje").value;
        if(mensaje.length < 1){
            validacionB.style.color = "red";
            validacionB.innerHTML = "Campo obligatorio";
            return false;
        }else{
            validacionB.innerHTML = "";
            return true;
        }

    }

    function comentar(){
        if(validarAutor() && validarMensaje()){  
            limpiarComentarios();  
            agregarComentario();
            if(comentarios.length>4){
                comentarios.shift();
            }
            crearDivisiones();
            return false;
        }else{
            return false;
        }
    }

    function agregarComentario(){
        var autor = document.getElementById("autor").value;
        var mensaje = document.getElementById("mensaje").value;
        var nuevoComentario = {
            titulo: ''+autor,
            descripcion: ''+mensaje
        }       
        comentarios.push(nuevoComentario);
    }

    function crearDivisiones(){
        for(var i=comentarios.length-1;i>-1;i--){
            var div = document.createElement('div');
            div.id = 'comentario'+i;
            div.innerHTML = '<h6>'+comentarios[i].titulo+'</h6><br><p>'+comentarios[i].descripcion+'</p>';
            div.className = 'nuevo comentario';
            document.getElementById('comentarios').appendChild(div);
        }
    }

    function limpiarComentarios(){
        for(var i=0;i<comentarios.length;i++){
            var div = document.getElementById('comentario'+i);
            document.getElementById('comentarios').removeChild(div);
        }
    }

