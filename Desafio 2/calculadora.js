    var arriba = document.getElementById("arriba");
    var abajo = document.getElementById("abajo");
    var cero = document.getElementById("cero");
    var uno = document.getElementById("uno");
    var tres = document.getElementById("tres");
    var cuatro = document.getElementById("cuatro");
    var cinco = document.getElementById("cinco");
    var seis = document.getElementById("seis");
    var siete = document.getElementById("siete");
    var ocho = document.getElementById("ocho");
    var nueve = document.getElementById("nueve");
    var operacion = document.getElementById("operacion");
    var operacionRealizada;
    var operadorPresionado = false;
    var igualPresionado = false;
    var fraccion = document.getElementById("division");
    var multiplicar = document.getElementById("multiplicar");
    var sumar = document.getElementById("sumar");
    var restar = document.getElementById("restar");
    var inverso = document.getElementById("inverso");
    var igual = document.getElementById("igual");
    var reiniciar = document.getElementById("limpiar");
    var dot = document.getElementById("punto");
    var borrar = document.getElementById("borrar");

    function nuevaOperacion(){
        abajo.innerHTML = "";
        operacion.innerHTML = "";
    }

    function limpiar(){
        arriba.innerHTML = "";
        abajo.innerHTML = "";
        operacion.innerHTML = ""; 
        operadorPresionado = false;
        igualPresionado = false;      
    }

    
    function suma(n1,n2){
        return parseFloat(n1)+parseFloat(n2);
    }

    function resta(n1,n2){
        return n1 - n2;
    }

    function multiplicacion(n1,n2){
        return n1*n2;
    }

    function division(n1,n2){
        return n1 / n2;
    }

    function inversoAditivo(n){
        return n*-1;
    }


    borrar.addEventListener("click",function(){
        if(operadorPresionado && abajo.innerHTML !== ""){
            var cadena = abajo.innerHTML;
            var texto = cadena.substring(0,cadena.length - 1);
            abajo.innerHTML = texto;
        }else if(arriba.innerHTML !== ""){
            var cadena = arriba.innerHTML;
            var texto = cadena.substring(0,cadena.length - 1);
            arriba.innerHTML = texto;   
        }
    });


    fraccion.addEventListener("click",function(){
        operacion.innerHTML = "&#247";
        operacionRealizada = "division";
        operadorPresionado= true;
    });

    multiplicar.addEventListener("click",function(){
        operacion.innerHTML = "x";
        operacionRealizada = "multiplicación";
        operadorPresionado= true;
    });

    sumar.addEventListener("click",function(){
        operacion.innerHTML = "+";
        operacionRealizada = "suma";
        operadorPresionado= true;
    });

    restar.addEventListener("click",function(){
        operacion.innerHTML = "-";
        operacionRealizada = "resta";
        operadorPresionado= true;
    });

    inverso.addEventListener("click",function(){
        if(operadorPresionado && abajo.innerHTML !== ""){
            abajo.innerHTML = inversoAditivo(abajo.innerHTML);
        }else if(!operadorPresionado && arriba.innerHTML !== ""){
            arriba.innerHTML = inversoAditivo(arriba.innerHTML);        
        }
    });

    dot.addEventListener("click",function(){
        if(operadorPresionado && abajo.innerHTML !== "" && !abajo.innerHTML.includes(".")){
            abajo.innerHTML += ".";
        }else if(!operadorPresionado && arriba.innerHTML !== "" && !arriba.innerHTML.includes(".")){
            arriba.innerHTML += ".";        
        }   
    });

    igual.addEventListener("click",function(){
        if(operacionRealizada !== "" && abajo.innerHTML !== ""){
            switch(operacionRealizada){
                case "division": arriba.innerHTML = division(arriba.innerHTML,abajo.innerHTML);
                break;
                case "multiplicación":arriba.innerHTML = multiplicacion(arriba.innerHTML,abajo.innerHTML);
                break;
                case "suma": arriba.innerHTML = suma(arriba.innerHTML,abajo.innerHTML);
                break;
                case "resta": arriba.innerHTML = resta(arriba.innerHTML,abajo.innerHTML);
                break;
            }
            operadorPresionado = false;
            igualPresionado = true;
            operacionRealizada = "";
            nuevaOperacion();
        }
    });

    reiniciar.addEventListener("click",function(){
        limpiar();
    });

    

    cero.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = cero.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }

    });

    uno.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = uno.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }
    })

    dos.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = dos.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }
    });

    tres.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = tres.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }
    });

    cuatro.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = cuatro.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }
    });

    cinco.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = cinco.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }
    });

    seis.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = seis.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }
    });

    siete.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = siete.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }
    });

    ocho.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = ocho.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }
    });

    nueve.addEventListener("click",function(){
        if(igualPresionado && !operadorPresionado && !arriba.innerHTML.includes(".")){
            arriba.innerHTML = "";
            igualPresionado = false;
        }
        var num = nueve.getAttribute('data-value');
        if(!operadorPresionado){
            arriba.innerHTML += num;
        }else{
            abajo.innerHTML += num;
        }
    });
